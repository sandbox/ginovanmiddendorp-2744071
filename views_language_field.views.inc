<?php

/**
 * Implements hook_views_data_alter().
 */
function views_language_field_views_data_alter(array &$data) {
    $data['node']['views_translation_status_field'] = array(
        'title' => t('Views Translation Status Field'),
        'field' => array(
            'title' => t('Views Translation Status Field'),
            'help' => t('Views Translation Status Field.'),
            'id' => 'views_translation_status_field',
        ),
    );
}