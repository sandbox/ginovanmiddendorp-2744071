<?php

/**
 * @file
 * Contains views_language_field.module..
 */

namespace Drupal\views_language_field\Plugin\views\field;

use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageDefault;
use Drupal\node\Entity\NodeType;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Language\Language;
use Drupal\Core\Link;
use Drupal\Core\Routing\LinkGeneratorTrait;

/**
 * Field handler to flag the node type.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("views_translation_status_field")
 */
class ViewsTranslationStatusField extends FieldPluginBase {

    /**
     * @{inheritdoc}
     */
    public function query() {
        // Leave empty to avoid a query on this field.
    }

    /**
     * Define the available options
     * @return array
     */
    protected function defineOptions() {
        $options = parent::defineOptions();
        return $options;
    }

    /**
     * Provide the options form.
     */
    public function buildOptionsForm(&$form, FormStateInterface $form_state) {
        parent::buildOptionsForm($form, $form_state);
    }

    /**
     * @{inheritdoc}
     */
    public function render(ResultRow $values) {

        // Get the available langcodes
        $node = $values->_entity;
        $langcodes = \Drupal::languageManager()->getLanguages();

        // Check if the current node is translated in each langcode.
        $items = [];
        foreach ($langcodes as $langcode => $language) {
            $language_name = $langcode;

            // Is not translated.
            if (!$node->hasTranslation($langcode)) {
                $class = 'item-grey';
            }
            else {
                // Is published.
                $translated_node = $node->getTranslation($langcode);
                if ($translated_node->isPublished()) {
                    $class = 'item-green';
                }
                else {
                    $class = 'item-orange';
                }
            }

            $items[] = SafeMarkup::format('<div class="' . $class . '">' . $language_name . '</div>', array());;
        }

        // Build the render array.
        $build = [
            '#theme' => 'item_list',
            '#items' => $items,
            '#type' => 'ul',
            '#attributes' => ['class' => 'language-list'],
        ];

        $build['#attached']['library'][] = 'views_language_field/translation-status';

        return $build;
    }
}
